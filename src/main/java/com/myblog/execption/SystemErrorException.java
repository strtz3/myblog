package com.myblog.execption;

import com.myblog.enums.SysEnum;

public class SystemErrorException extends RuntimeException {

    public SystemErrorException(SysEnum sysEnum) {
        super(sysEnum.getMessage());
    }

}
