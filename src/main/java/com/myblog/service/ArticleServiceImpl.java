package com.myblog.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myblog.mapper.ArticleMapper;
import com.myblog.entity.Article;
import com.myblog.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service
@Transactional
@CacheConfig(cacheNames = "BLOG:SESSION_KEY_IN_ARTICLE")
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private RedisUtil redisUtil;

    @Cacheable(key = "'LIST_P'+ #page +'L'+ #limit")
    @Override
    public PageInfo<Article> list(Integer page, Integer limit) {
        //不分页
        List<Article> list = articleMapper.list();
        //从redis中取出阅读量
        if (redisUtil.hentries("BLOG:ARTICLE_VIEW").size() <= 0) {
            for (Article article : list) {
                redisUtil.hset("BLOG:ARTICLE_VIEW", String.valueOf(article.getId()), article.getViews());
            }
        }
        //从redis取出文章排序
        if (redisUtil.zrange("BLOG:ARTICLE_RANK", 0, -1).size() <= 0) {
            for (Article article : list) {
                redisUtil.zadd("BLOG:ARTICLE_RANK", article, article.getCreateTime().getTime());
            }
        }
        //分页
        if (page != null && limit != null) {
            PageHelper.startPage(page, limit);
        }
        return new PageInfo<Article>(articleMapper.list());
    }

    @CacheEvict(allEntries = true)
    @Override
    public void save(Article article) {
        if (article.getId() != null) {
            article.setUpdateTime(new Date());
            articleMapper.update(article);
        } else {
            article.setCreateTime(new Date());
            article.setUpdateTime(new Date());
            articleMapper.save(article);
        }
    }

    @CacheEvict(allEntries = true)
    @Override
    public void del(Article article) {
        //删除中间表
        articleMapper.delMiddle(article.getId());
        //删除文章
        articleMapper.deleteById(article.getId());
    }

    @Cacheable(key = "'LIST_CID_'+ #a2 + '_L' +#page +'L'+ #limit", unless = "#result == null")
    @Override
    public PageInfo<Article> listByCid(Integer page, Integer limit, Integer categoryId) {
        PageHelper.startPage(page, limit);
        return new PageInfo<Article>(articleMapper.listByCid(categoryId));
    }

    @Cacheable(key = "'LIST_LID'+ #a2 + '_P' +#page +'L'+ #limit", unless = "#result == null")
    @Override
    public PageInfo<Article> listByLid(Integer page, Integer limit, Integer labelId) {
        PageHelper.startPage(page, limit);
        return new PageInfo<Article>(articleMapper.listByLid(labelId));
    }

    @Cacheable(key = "'LIST_AID'+ #a0 + '_L' +#page +'L'+ #limit", unless = "#result == null")
    @Override
    public Article listByAid(Integer articleId) {
        Article article = articleMapper.listByAid(articleId);
        return article;
    }

    @Cacheable(key = "'LIST_SEARCH_'+ #keyword+'P'+ #page +'L'+ #limit", unless = "#result == null")
    @Override
    public PageInfo<Article> searchByKeyword(Integer page, Integer limit, String keyword) {
        PageHelper.startPage(page, limit);
        return new PageInfo<Article>(articleMapper.searchByKeyword("%" + keyword + "%"));
    }

    @Cacheable(key = "'total'", unless = "#result == null")
    @Override
    public Integer total() {
        return articleMapper.total();
    }

    @CacheEvict(allEntries = true) //此注解会清除cacheNames下所有缓存
    @Override
    public void insertMiddleTable(Integer lid, Integer aid) {

        if (aid != null && lid != null) {
            //判断是否已经存在
            if (articleMapper.findMiddle(lid, aid) == null) {
                articleMapper.insertMiddleTable(lid, aid);
            }
        }
    }
}
