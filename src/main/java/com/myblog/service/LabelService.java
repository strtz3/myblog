package com.myblog.service;

import com.myblog.entity.Label;

import java.util.List;

public interface LabelService {
    List<Label> list();

    Label listById(Integer id);

    void save(Label label);

    void del(Integer lid);

    Label findByName(Label label);

    Integer total();
}
