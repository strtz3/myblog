package com.myblog.service;

import com.github.pagehelper.PageInfo;
import com.myblog.entity.Article;

public interface ArticleService {

    PageInfo<Article> list(Integer page, Integer limit);

    PageInfo<Article> listByCid(Integer page, Integer limit, Integer categoryId);

    PageInfo<Article> listByLid(Integer page, Integer limit, Integer labelId);

    Integer total();

    Article listByAid(Integer articleId);

    PageInfo<Article> searchByKeyword(Integer page, Integer limit, String keyword);

    void save(Article article);

    void del(Article article);

    void insertMiddleTable(Integer lid, Integer aid);
}
