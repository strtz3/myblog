package com.myblog.service;

import com.qiniu.common.QiniuException;

import java.io.File;
import java.util.Map;

public interface QiniuService {


    /**
     * 多文件上传
     * @param file
     * @return
     * @throws QiniuException
     */
    Map uploadFile(File file) throws QiniuException;

}

