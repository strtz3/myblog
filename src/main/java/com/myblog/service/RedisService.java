package com.myblog.service;

import com.myblog.entity.Article;

import java.util.List;

public interface RedisService {

    void setArticleRank(List<Article> articleList);

    void setArticleView(List<Article> articleList);

}
