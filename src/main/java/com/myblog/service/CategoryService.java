package com.myblog.service;

import com.myblog.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> list();

    Category listByCid(Integer id);

    void save(Category category);

    void del(Integer id);

    Integer total();
}
