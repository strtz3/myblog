package com.myblog.service;

import com.myblog.mapper.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
@CacheConfig(cacheNames = "BLOG:SESSION_KEY_IN_COMMENT")
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Cacheable(key = "'total'", unless = "#result == null")
    @Override
    public Integer total() {
        return commentMapper.total();
    }
}
