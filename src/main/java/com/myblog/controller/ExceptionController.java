package com.myblog.controller;


import com.myblog.enums.SysEnum;
import com.myblog.execption.SystemErrorException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class ExceptionController {

    @RequestMapping("/verify/null")
    public void verify_null(HttpServletResponse response, HttpSession session) throws IOException {
        throw new SystemErrorException(SysEnum.VERIFY_NULL);
    }

    @RequestMapping("/verify/error")
    public void verify_error(HttpServletResponse response, HttpSession session) throws IOException {
        throw new SystemErrorException(SysEnum.VERIFY_ERROR);
    }
}
