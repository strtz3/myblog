package com.myblog.controller.admin;

import com.myblog.service.ArticleService;
import com.myblog.service.CategoryService;
import com.myblog.service.LabelService;
import com.myblog.enums.SysEnum;
import com.myblog.execption.SystemErrorException;
import com.myblog.entity.Article;
import com.myblog.entity.Label;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@ApiModel("文章控制层")
@Controller
@RequestMapping("/admin")
public class ArticleController {

    private final static String ADMIN_BLOG = "admin/blog";
    private final static String ADMIN_ADD_BLOG = "admin/blog-input";
    private final static String REDIRECT_LIST = "redirect:/" + ADMIN_BLOG;

    @Autowired
    private ArticleService articleService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private LabelService labelService;

    @ApiOperation("跳转到博客发布页面")
    @GetMapping("/jumpBlog")
    public String addBlog(Model model, Integer id) {
        if (id != null) {
            model.addAttribute("pojo", articleService.listByAid(id));
        }
        model.addAttribute("categoryList", categoryService.list());
        return ADMIN_ADD_BLOG;
    }

    @ApiOperation("查询全部文章")
    @GetMapping("/blog")
    public String list(Model model) {
        model.addAttribute("pageInfo", articleService.list(null, null));
        return ADMIN_BLOG;
    }

    @ApiOperation("保存文章")
    @PostMapping("/blog")
    public String save(@Validated Article article, BindingResult bindingResult, String labelName) {
        //检查是否为空
        if (bindingResult.hasErrors()) {
            throw new SystemErrorException(SysEnum.ILLEGAL_ERROR);
        }

        //添加文章,返回自增ID
        articleService.save(article);
        //标签判断：此处只写了添加单个标签的情况，数据库和实体类设置是多对多的关系
        Label label = new Label();
        if (labelName != null) {
            label.setName(labelName);
            //判断是否存在
            Label inquiryLabel = labelService.findByName(label);
            if (labelService.findByName(label) == null) {
                //添加标签,返回自增ID
                labelService.save(label);
                //如果标签不存在，添加中间表
                articleService.insertMiddleTable(label.getId(), article.getId());
            } else {
                //如果标签存在，添加中间表
                articleService.insertMiddleTable(inquiryLabel.getId(), article.getId());
            }
        }

        return REDIRECT_LIST;
    }

    @ApiOperation("删除文章")
    @DeleteMapping("/blog")
    public String del(Article article) {
        articleService.del(article);
        return REDIRECT_LIST;
    }
}
