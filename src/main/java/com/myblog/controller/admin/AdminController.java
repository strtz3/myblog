package com.myblog.controller.admin;

import com.myblog.service.ArticleService;
import com.myblog.service.CategoryService;
import com.myblog.service.CommentService;
import com.myblog.service.LabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminController {

    private final static String ADMIN_INDEX = "admin/index";
    private final static String ADMIN_BLOG = "admin/blog";

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ArticleService articleService;
    @Autowired
    private LabelService labelService;
    @Autowired
    private CommentService commentService;

    @GetMapping({"/admin/index", "/admin"})
    public String index(Model model) {
        //分类的数量
        model.addAttribute("categoryTotal", categoryService.total());
        //博客数量
        model.addAttribute("articleTotal", articleService.total());
        //标签数量
        model.addAttribute("labelTotal", labelService.total());
        //评论数量
        model.addAttribute("commentTotal", commentService.total());
        return ADMIN_INDEX;
    }

}
