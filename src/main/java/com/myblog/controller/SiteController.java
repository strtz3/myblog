package com.myblog.controller;

import com.github.pagehelper.PageInfo;
import com.myblog.entity.Article;
import com.myblog.enums.SysEnum;
import com.myblog.execption.SystemErrorException;
import com.myblog.service.ArticleService;
import com.myblog.service.CategoryService;
import com.myblog.service.LabelService;
import com.myblog.utils.MarkdownUtil;
import com.myblog.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class SiteController {

    private final static String INDEX = "site/index";
    private final static String REFRESH_INDEX = "site/index::blog-list";
    //private final static String REFRESH_BLOG_DETAIL = "site/common/com-template::article-show";
    private final static String BLOG_SHOW = "site/blog-show";
    private final static String PAGE = "1";
    private final static String LIMIT = "5";

    @Autowired
    private ArticleService articleService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private LabelService labelService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 查询页面信息
     *
     * @param page
     * @param limit
     * @param model
     * @return
     */
    @GetMapping({"", "/", "/index"})
    public String articleDetail(@RequestParam(required = true, defaultValue = PAGE) Integer page,
                                @RequestParam(required = true, defaultValue = LIMIT) Integer limit, Model model) {

        //获取最新文章
        model.addAttribute("lastArticle", new ArrayList(redisUtil.zreverseRange("BLOG:ARTICLE_RANK", 0, -1)));
        //全部查询
        model.addAttribute("pageInfo", articleService.list(page, limit));
        //查询分类
        model.addAttribute("categoryList", categoryService.list());
        //查询标签
        model.addAttribute("labelList", labelService.list());
        return INDEX;
    }

    /**
     * 根据分类、标签查询文章（局部刷新）
     *
     * @param page
     * @param limit
     * @param categoryId
     * @param labelId
     * @param model
     * @return
     */
    @GetMapping("/blog")
    public String refresh(@RequestParam(required = true, defaultValue = PAGE) Integer page,
                          @RequestParam(required = true, defaultValue = LIMIT) Integer limit,
                          @RequestParam(value = "cid", required = false) Integer categoryId,
                          @RequestParam(value = "lid", required = false) Integer labelId,
                          @RequestParam(value = "aid", required = false) Integer articleId,
                          @RequestParam(value = "keyword", required = false) String keyword, Model model) {

        //文章刷新
        if (articleId != null) {
            Article article = articleService.listByAid(articleId);
            if (article == null) {
                throw new SystemErrorException(SysEnum.ILLEGAL_ARTICLE);
            }
            article.setContent(MarkdownUtil.markdownToHtmlExtensions(article.getContent()));
            model.addAttribute("article", article);
            //阅读量递增
            redisUtil.hincr("BLOG:ARTICLE_VIEW", String.valueOf(article.getId()), 1);
            //查询阅读量
            model.addAttribute("view", redisUtil.hget("BLOG:ARTICLE_VIEW", String.valueOf(articleId)));
            return BLOG_SHOW;
        }
        //首页刷新
        PageInfo<Article> pageInfoArticle = null;
        if (categoryId != null) {
            //根据分类查询
            pageInfoArticle = articleService.listByCid(page, limit, categoryId);
            //根据分类查询后，分页需要的参数
            model.addAttribute("h_cid", categoryId);
        } else if (labelId != null) {
            //根据标签查询
            pageInfoArticle = articleService.listByLid(page, limit, labelId);
            //根据标签查询后，分页需要的参数
            model.addAttribute("h_lid", labelId);
        } else if (keyword != null && !keyword.equals("")) {
            //搜索
            pageInfoArticle = articleService.searchByKeyword(page, limit, keyword);
        } else {
            //全部查询
            pageInfoArticle = articleService.list(page, limit);
        }
        model.addAttribute("pageInfo", pageInfoArticle);
        return REFRESH_INDEX;
    }

}
