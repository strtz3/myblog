package com.myblog.controller;

import com.myblog.utils.RedisUtil;
import com.myblog.utils.VerifyCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
public class VerifyCodeController {
    @Autowired
    private RedisUtil redisUtil;

    @GetMapping("/getVerifyCode")
    public void verifyCode(HttpServletResponse response, HttpSession session) throws IOException {
        int w = 120, h = 40;
        String verifyCode = VerifyCodeUtil.generateVerifyCode(4);
        VerifyCodeUtil.outputImage(w, h, response.getOutputStream(), verifyCode.toUpperCase());
        //缓存放入并设置时间
        redisUtil.set("BLOG:verifyCode:" + verifyCode, verifyCode,60);
    }
}
