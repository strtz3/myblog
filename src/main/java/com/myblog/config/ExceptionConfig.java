package com.myblog.config;

import com.myblog.execption.SystemErrorException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice //拦截标注@controller的产生的异常
//@Slf4j //配置了springboot全局配置抓取controller的warn日志，所以此处不需要日志
public class ExceptionConfig {


    @ExceptionHandler(Exception.class)
    //@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) 标注状态码
    public String errorHandler(Exception exception, HttpServletRequest request, Model model) throws Exception {
        //抓取错误信息
        String url = request.getRequestURL().toString();
        model.addAttribute("exception", exception.getMessage());
        //判断异常类型
        if (exception instanceof SystemErrorException) {
            return "error/catchError";
        }
        throw exception;
    }
}
