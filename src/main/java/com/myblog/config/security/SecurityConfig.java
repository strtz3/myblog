package com.myblog.config.security;

import com.myblog.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    UserService userService;

    @Autowired
    private VerifyCodeFilter verifyCodeFilter;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 授权
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //关闭跨站检测
        http.csrf().disable();
        //同域名的iframe允许
        http.headers().frameOptions().sameOrigin();

        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN");

        http.addFilterBefore(verifyCodeFilter, UsernamePasswordAuthenticationFilter.class) //图片验证
                .formLogin() //无权限的情况跳转登录页面
                .loginPage("/login") //自定义登录页面、post登录提交数据
                .defaultSuccessUrl("/admin", true) //登录成功跳转页面，使用该方法的原因是另一个方法不跳
                .usernameParameter("username") //自定义接收前端的参数
                .passwordParameter("password");
        //注销
        http.logout()
                .logoutSuccessUrl("/admin") //注销成功后跳转的url
                .deleteCookies("JESSIONID"); //退出清空cookie
        //记住密码、cookie形式
        http.rememberMe()
                .rememberMeParameter("remember"); //自定义接收前端的参数
    }

    /**
     * 认证
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 使用数据库中用户数据，可以自定义的加密方法
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
        //将用户信息写入内存
        /*auth.inMemoryAuthentication().passwordEncoder(passwordEncoder)
                .withUser("admin").password(passwordEncoder.encode("admin")).roles("ADMIN");*/
    }


    /**
     * 自带加密
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123456");
        System.out.println(encode);
    }
}
