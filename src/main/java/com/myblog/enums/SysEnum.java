package com.myblog.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SysEnum implements ErrorCode {
    SYS_ERROR(2001, "服务冒烟了，要不然你稍后再试试"),
    ILLEGAL_ERROR(2001, "参数不合法"),
    ILLEGAL_CATEGORY(2001, "该分类不存在"),
    ILLEGAL_LABEL(2001, "该标签不存在"),
    ILLEGAL_ARTICLE(2001, "该文章不存在"),
    ILLEGAL_USER(2001, "该用户不存在"),
    VERIFY_ERROR(2001, "图片码错误"),
    VERIFY_NULL(2001, "图片码为空");

    private Integer code;
    private String message;
}

