package com.myblog.enums;

public interface ErrorCode {
    String getMessage() ;
    Integer getCode();
}
