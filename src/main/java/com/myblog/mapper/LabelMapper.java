package com.myblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.myblog.entity.Label;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LabelMapper extends BaseMapper<Label> {

    List<Label> listByAid(Integer aid);

    List<Label> list();

    Label listByLid(Integer id);

    Integer save(Label label);

    Integer total();

    void delMiddle(Integer id);
}
