package com.myblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.myblog.entity.Article;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleMapper extends BaseMapper<Article> {

    List<Article> list();

    Integer calculateByLid(Integer Lid);

    List<Article> listByLid(Integer Lid);

    Integer calculateByCid(Integer cid);

    List<Article> listByCid(Integer cid);

    Article listByAid(Integer articleId);

    List<Article> searchByKeyword(String keyword);

    void save(Article article);

    Integer total();

    void insertMiddleTable(Integer lid,Integer aid);

    Object findMiddle(Integer lid, Integer aid);

    void delMiddle(Integer id);

    void update(Article article);
}
