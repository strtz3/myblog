package com.myblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.myblog.entity.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMapper extends BaseMapper<Role> {
    List<Role> listById(Integer id);
}
