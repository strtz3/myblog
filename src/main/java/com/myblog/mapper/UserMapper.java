package com.myblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.myblog.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {

}
