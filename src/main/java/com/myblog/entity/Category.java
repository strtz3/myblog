package com.myblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Data
@TableName("tb_category")
@ApiModel("分类实体类")
public class Category implements Serializable {
    @ApiModelProperty("分类ID")
    @TableId(type = IdType.INPUT)
    private Integer id;
    @ApiModelProperty("分类名称")
    @NotBlank
    private String name;
    @ApiModelProperty("分类下的文章数量")
    @TableField(exist = false)
    private Integer blogNum;
    @ApiModelProperty("文章集合对象、一对多关系映射")
    @TableField(exist = false)
    private List<Article> blogList;
}
