package com.myblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@TableName("tb_role")
@Data
@ApiModel("角色实体类")
public class Role implements Serializable {
    @ApiModelProperty("唯一标识")
    @TableId(type = IdType.INPUT)
    private Integer id;
    @ApiModelProperty("角色名")
    private String roleName;
    @ApiModelProperty("角色详情")
    private String comment;
    @ApiModelProperty("用户集合：多对多关系映射")
    @TableField(exist = false)
    List<User> users;
}
