package com.myblog.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Log implements Serializable {
    private String url;
    private String ip;
    private String classMethod;
    private Object[] args;
}
