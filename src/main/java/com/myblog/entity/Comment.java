package com.myblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Data
@TableName("tb_comment")
@ApiModel("评论实体类")
public class Comment implements Serializable {
    @ApiModelProperty("评论ID")
    @TableId(type = IdType.INPUT)
    private Integer id;
    @ApiModelProperty("评论内容")
    @NotBlank
    private String content;
    @ApiModelProperty("评论的文章ID")
    private Integer articleId;
    @ApiModelProperty("评论的父节点")
    private Integer parentId;
    @ApiModelProperty("创建时间")
    private Date createTime;
    //评论的子节点
}
