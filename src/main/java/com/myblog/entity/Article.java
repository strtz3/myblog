package com.myblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@ApiModel("文章实体类")
@TableName("tb_article")
@Data
public class Article implements Serializable {
    @ApiModelProperty("文章ID")
    @TableId(type = IdType.INPUT)
    private Integer id;
    @ApiModelProperty("文章标题")
    @NotBlank
    private String title;
    @ApiModelProperty("首图地址")
    @URL
    private String firstPicture;
    @ApiModelProperty("文章描述")
    @NotBlank
    private String description;
    @ApiModelProperty("文章内容")
    private String content;
    @ApiModelProperty("发布状态")
    private Boolean published;
    @ApiModelProperty("浏览数")
    private Integer views;
    @ApiModelProperty("评论数量")
    @TableField(exist = false)
    private Integer commentNum;
    @ApiModelProperty("创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty("最后更新时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @ApiModelProperty("分类ID")
    private Integer categoryId;
    @ApiModelProperty("分类对象、一对一关系映射")
    @TableField(exist = false)
    private Category category;
    @ApiModelProperty("标签集合、多对多关系映射")
    @TableField(exist = false)
    private List<Label> labels;
}