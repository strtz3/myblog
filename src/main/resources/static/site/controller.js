layui.use(['jquery', 'layer', 'table', 'form'], function () {
    var layer = layui.layer, table = layui.table, form = layui.form, $ = layui.jquery;

    $('#jump-login').click(function () {
        layer.open({
            type: 2,
            content: '/login',
            title: '后台登录',
            shadeClose: true,
            shade: false,
            maxmin: true, //开启最大化最小化按钮
            area: ['380px', '80%']
        });
    })
})

//局部刷新
function refresh(cid, lid, page) {
    $.ajax({
        url: '/blog',
        type: 'GET',
        data: {
            cid: cid,
            lid: lid,
            page: page,
            keyword: $("#keyword").val()
        },
        success: function (data) {
            //将返回结果添加到标签
            $("#refresh_blog").html(data)
            //跳转回顶部
            window.scrollTo(0, 600)
        }
    })
}