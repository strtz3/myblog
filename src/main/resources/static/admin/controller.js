layui.use(['jquery', 'layer', 'table', 'form'], function () {
    var layer = layui.layer, table = layui.table, form = layui.form, $ = layui.jquery;
//===================================博客============================================================================
    //保存博客
    $('.blog-submit-btn').click(function () {
        $.ajax({
            url: '/admin/blog',
            method: 'POST',
            data: $('#blog-form').serialize(),
            success: function (data) {
                //刷新页面
                parent.location.href = "/admin/blog"
                layer.msg('操作成功')
            }
        })
    })

    //删除博客
    $('.delete-blog').click(function () {
        //获取id
        let id = $(this).attr("postid")
        layer.confirm('is not?', {icon: 3, title: '提示'}, function (index) {
            //do something
            layer.close(index);
            $.ajax({
                url: '/admin/blog',
                method: 'POST',
                data: {
                    '_method': 'DELETE',
                    id: id
                },
                success: function (data) {
                    //刷新页面
                    parent.location.href = '/admin/blog'
                    layer.msg('操作成功')
                }
            })
        });
    })

//===================================分类============================================================================
    $('.update-category').click(function () {
        //获取id
        let id = $(this).attr("postid")
        //空值判断
        if (typeof (id) == "undefined") {
            id = ""
            //清空数据
            $('#category-form')[0].reset()
            //弹出层
            addCategory()
        } else {
            //数据回显
            $.ajax({
                url: '/admin/category',
                method: 'POST',
                data: {
                    '_method': 'PUT',
                    id: id
                },
                success: function (data) {
                    //弹出层
                    updateCategory(data)
                }
            })
        }
    })

    //添加
    function addCategory() {
        layer.open({
            type: 1,
            content: $('#categoryForm'),
            title: '分类添加',
            shadeClose: true,
            shade: false,
            maxmin: true, //开启最大化最小化按钮
            area: ['380px']
        });
    }

    //更新
    function updateCategory(data) {
        layer.open({
            type: 1,
            content: $('#categoryForm'),
            title: '分类编辑',
            shadeClose: true,
            shade: false,
            maxmin: true, //开启最大化最小化按钮
            area: ['380px'],
            success: function () {
                $('#name').val(data.name)
                $('#id').val(data.id)
            }
        });
    }

    //监听提交
    form.on('submit(lay_categoryForm)', function (data) {
        $.ajax({
            url: '/admin/category',
            data: data.field,
            method: 'POST',
            success: function (data) {
                layer.closeAll()
                layer.msg('操作成功')
                //刷新页面
                parent.location.reload()
            }
        })
        return false;
    });

    //删除
    $('.delete-category').click(function () {
        //获取id
        let id = $(this).attr("postid")
        //空值判断
        if (typeof (id) == "undefined") {
            id = ""
        }
        layer.confirm('确定删除吗?', {icon: 3, title: '提示'}, function (index) {
            layer.close(index);
            $.ajax({
                url: '/admin/category',
                method: 'POST',
                data: {
                    '_method': 'DELETE',
                    id: id
                },
                success: function (data) {
                    layer.closeAll()
                    layer.msg('操作成功')
                    //刷新页面
                    parent.location.reload()
                }
            })
        });
    })
//===================================标签============================================================================
    $('.update-label').click(function () {
        //获取id
        let id = $(this).attr("postid")
        //空值判断
        if (typeof (id) == "undefined") {
            id = ""
            //清空数据
            $('#label-form')[0].reset()
            //弹出层
            addLabel()
        } else {
            //数据回显
            $.ajax({
                url: '/admin/label',
                method: 'POST',
                data: {
                    '_method': 'PUT',
                    id: id
                },
                success: function (data) {
                    //弹出层
                    updateLabel(data)
                }
            })
        }
    })

//添加
    function addLabel() {
        layer.open({
            type: 1,
            content: $('#labelForm'),
            title: '标签添加',
            shadeClose: true,
            shade: false,
            maxmin: true, //开启最大化最小化按钮
            area: ['380px']
        });
    }

//更新
    function updateLabel(data) {
        layer.open({
            type: 1,
            content: $('#labelForm'),
            title: '标签编辑',
            shadeClose: true,
            shade: false,
            maxmin: true, //开启最大化最小化按钮
            area: ['380px'],
            success: function () {
                $('#name').val(data.name)
                $('#id').val(data.id)
            }
        });
    }

    //监听提交
    form.on('submit(lay_labelyForm)', function (data) {
        $.ajax({
            url: '/admin/label',
            method: 'POST',
            data: data.field,
            success: function (data) {
                layer.closeAll()
                layer.msg('操作成功')
                //刷新页面
                parent.location.reload()
            }
        })
        return false;
    });

    //删除
    $('.delete-label').click(function () {
        //获取id
        let id = $(this).attr("postid")
        //空值判断
        if (typeof (id) == "undefined") {
            id = ""
        }
        layer.msg(id)
        layer.confirm('确认删除吗?', {icon: 3, title: '提示'}, function (index) {
            layer.close(index);
            $.ajax({
                url: '/admin/label',
                method: 'POST',
                data: {
                    '_method': 'DELETE',
                    id: id
                },
                success: function (data) {
                    layer.closeAll()
                    layer.msg('操作成功')
                    //刷新页面
                    parent.location.reload()
                }
            })
        });
    })
})