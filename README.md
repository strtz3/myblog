# 个人博客

#### 介绍
学习过程中写的项目

#### 软件架构
前端：layui
后端技术：springboot、mybatis、myabtis plus
中间件：redis
部署 Tomcat、Nginx、七牛云、阿里云、druid监控


#### 安装教程

1.  springboot项目导入即可

#### 使用说明

1.  application.yml中填上配置信息：mysql、redis、七牛云、druid监控日志账号密码

#### 参与贡献

1.  博客模板由【犬山葵啊】提供


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
